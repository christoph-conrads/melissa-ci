#!/bin/bash

# Copyright 2020, 2021 Institut national de recherche en informatique et en automatique (Inria)
#
# The code in this file is based on code found on GitLab.com.
# URL: https://docs.gitlab.com/runner/executors/custom_examples/lxd.html 
# Date: 2020-10-02

set -e
set -o pipefail
set -u


currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "${currentDir}/base.sh"

# trap any error, and mark it as a system failure.
trap "exit $SYSTEM_FAILURE_EXIT_CODE" ERR


# make sure the image identifier is not garbled non-sense like `ubuntu-debian`
check_image_identifier() {
    local image="${1:?}"

	if ! $(lxc image info -- "$image" >/dev/null 2>/dev/null)
	then
		>/dev/stderr echo "LXD image '$image' not found"
		exit $BUILD_FAILURE_EXIT_CODE
	fi
}



# TODO: look for existing containers so that we can avoid downloading and
# updating images every time
start_container () {
    local image="${1:?}"

    if $(lxd_container_exists_p "$CONTAINER_ID")
	then
        echo "deleting existing container '$CONTAINER_ID'"
        lxc delete --force -- "$CONTAINER_ID"
    fi

    lxc launch -- "${image}" "$CONTAINER_ID"

    # Wait for container to start
    for i in $(seq 1 10); do
        if [[ "$image" =~ alpine ]]; then
            runlevel="$(lxc exec "$CONTAINER_ID" -- rc-status --runlevel)"
            if [ "$runlevel" = 'default' ]; then
                break
            fi
        elif [[ "$image" =~ devuan ]]; then
            # ignore errors because runlevel has exit status 1 during boot
            set +e
            runlevel="$(lxc exec "$CONTAINER_ID" -- runlevel)"
            set -e
            if [ "$runlevel" = 'N 2' ]; then
                break
            fi
        elif lxc exec "$CONTAINER_ID" -- sh -c "systemctl isolate multi-user.target" >/dev/null 2>/dev/null; then
            break
        fi

        if [ "$i" == "10" ]; then
            echo 'Waited for 10 seconds to start container, exiting..'
            # Inform GitLab Runner that this is a system failure, so it
            # should be retried.
            exit "$SYSTEM_FAILURE_EXIT_CODE"
        fi

        sleep 1s
    done
    if [[ "$image" =~ alpine ]] || [[ "$image" =~ centos ]]; then
	# waiting here avoids DNS issues where the GitLab server ends up being
	# unreachable
        sleep 1s
    fi
}

install_dependencies () {
    local image="${1:?}"
}


echo "Running in $CONTAINER_ID"

check_image_identifier "$JOB_IMAGE"
start_container "$JOB_IMAGE"
install_dependencies "$JOB_IMAGE"

if $(local_execution_p)
then
	# The mounted directory will be read-only unless the subordinate IDs were
	# added to /etc/subgid, /etc/subuid and ID maps defined in the LXD config.
	lxc config device add "$CONTAINER_ID" git-repo disk \
		source="$CUSTOM_ENV_CI_REPOSITORY_URL" \
		path="$CUSTOM_ENV_CI_REPOSITORY_URL"
fi
