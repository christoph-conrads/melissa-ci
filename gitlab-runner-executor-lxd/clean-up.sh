#!/bin/bash

# Copyright 2020 Institut national de recherche en informatique et en automatique (Inria)
#
# The code in this file is based on code found on GitLab.com.
# URL: https://docs.gitlab.com/runner/executors/custom_examples/lxd.html 
# Date: 2020-10-02

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "${currentDir}/base.sh"

# Explicitly stop all containers instead of using `lxc delete -f`. Ephemeral
# containers are deleted this way and persistent containers may take a long
# time to stop and `lxc delete` may time out.
#
# Time-out can happen, for example, if the container storage is still busy.
# Then one can find error messages like the following in the LXD log
# (formatting mine):
#
#   zfs destroy failed: cannot destroy 'default/containers/container-id':
#   dataset is busy
#
# We suffered from the problem when using a ZFS image on an encrypted ext4
# disk.
#
# Stop containers even on error to stop running processes in the container.
lxc stop -- "$CONTAINER_ID"

if [ "$CUSTOM_ENV_CI_JOB_STATUS" = 'success' ]
then
	if $(lxd_container_exists_p "$CONTAINER_ID")
	then
		echo "container '$CONTAINER_ID' not ephemeral. deleting..."
		lxc delete --force -- "$CONTAINER_ID"
	fi
fi
