# LXD as GitLab Runner Executor

This repository contains the scripts needed to use LXD as executor for GitLab Runner.

The code below shows how to modify the GitLab Runner config in `/etc/gitlab-runner/config.toml`. Note that LXD builds are run as a normal user; the build and cache directory must be writable for this user.

```toml
[[runners]]
  name = "runner-name"
  url = "runner-url"
  token = "runner-token"
  executor = "custom"
  builds_dir = "/tmp/builds"
  cache_dir = "/tmp/cache"
  [runners.custom]
    prepare_exec = "/path/to/gitlab-runner-executor-lxd/prepare.sh"
    run_exec = "/path/to/gitlab-runner-executor-lxd/run.sh"
    cleanup_exec = "/path/to/gitlab-runner-executor-lxd/clean-up.sh"
```
