#!/bin/bash

# Copyright 2020 Institut national de recherche en informatique et en automatique (Inria)
#
# The code in this file is based on code found on GitLab.com.
# URL: https://docs.gitlab.com/runner/executors/custom_examples/lxd.html 
# Date: 2020-10-02

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "${currentDir}/base.sh"

lxc exec "$CONTAINER_ID" -- sudo --login --user john -- /bin/bash <"${1}"

if [ $? -ne 0 ]; then
    exit $BUILD_FAILURE_EXIT_CODE
fi
