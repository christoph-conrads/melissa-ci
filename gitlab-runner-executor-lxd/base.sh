#!/bin/bash

# Copyright 2020 Institut national de recherche en informatique et en automatique (Inria)
#
# The code in this file is based on code found on GitLab.com.
# URL: https://docs.gitlab.com/runner/executors/custom_examples/lxd.html
# Date: 2020-10-02

lxd_container_exists_p() {
	local container_id="${1:?}"

    lxc info -- "$container_id" >/dev/null 2>/dev/null
}

local_execution_p() {
	test -z "${CUSTOM_ENV_CI_SERVER_URL+X}"
}

# In a local execution, runner ID, project ID, job ID, and so on are all zero.
# Thus, a different container ID is generated.
#
# Beware:
# This script is invoked multiple times and must output the same variable
# values every time!
if $(local_execution_p)
then
	CONTAINER_ID="runner-$USER-$(echo $CUSTOM_ENV_CI_COMMIT_SHA | cut -c -8)"
else
	CONTAINER_ID="runner-$CUSTOM_ENV_CI_RUNNER_ID-project-$CUSTOM_ENV_CI_PROJECT_ID-job-$CUSTOM_ENV_CI_JOB_ID-$CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID"
fi

# the environment variable CI_JOB_IMAGE does not exist unless there is an image
# field in `.gitlab-ci.yml`
JOB_IMAGE="${CUSTOM_ENV_CI_JOB_IMAGE:-datamove/debian/9/amd64}"
