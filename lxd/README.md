# LXD Images

## File Names

**Files must be named after the distribution in lower case.**

Right:
* `debian.yml`
* `centos.yaml`

Wrong:
* `Debian.yml`
* `alpine-3.12.yml`
