#!/bin/bash

# Copyright 2020 Inria (https://www.inria.fr/)

# This script sets up QEMU+KVM and installs Alpine Linux 3.12.

set -e
set -o pipefail
set -u


die() {
	>/dev/stderr echo $@
	exit 1
}

warn() {
	>/dev/stderr echo $@
}

inform() {
	echo $@
}


# parse command line arguments
distribution="${1:-debian}"

if [ "$distribution" != alpine -a "$distribution" != debian ]
then
	die "unknown distribution '$distribution'"
fi


#
if [ $(lsb_release --short --id) != 'Ubuntu' ]
then
	warn 'This script has only been tested on Ubuntu 18.04 LTS'
fi


if ! $(lsmod | fgrep kvm >/dev/null)
then
	die 'Kernel module `kvm` unavailable or not loaded'
fi

if ! $(dpkg -l | fgrep qemu-kvm >/dev/null)
then
	echo 'Package qemu-kvm is not installed. Installing now...'
	sudo apt-get --quiet --yes install qemu-kvm
fi

if ! $(groups | fgrep kvm >/dev/null)
then
	echo "User $USER is not a member of kvm group. Adding now..."
	sudo adduser --quiet "$USER" kvm
fi


# install linux
case "$distribution" in
alpine)
	inform 'Installing Alpine Linux 3.12.0'

	iso_url='http://dl-cdn.alpinelinux.org/alpine/v3.12/releases/x86_64/alpine-virt-3.12.0-x86_64.iso'
	;;

debian)
	inform 'Installing Debian Linux 10.6.0 (Buster)'

	iso_url='https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-10.6.0-amd64-netinst.iso'
	;;

*)
	die "unknown distribution '$distribution'"
esac

iso="$(basename $iso_url)"
iso_path="$HOME/Downloads/$iso"
img="$(basename $iso .iso).img"


if [ ! -f "$iso_path" ]
then
	wget -O "$iso_path" -- "$iso_url"
fi

qemu-img create -f qcow2 "$img" 15G 
exec qemu-system-x86_64 \
	-enable-kvm \
	-cpu host \
	-m 4G \
	-drive file="$img",if=virtio \
	-netdev user,id=vmnic,hostname="$distribution" \
	-device virtio-net,netdev=vmnic \
	-device virtio-rng-pci \
	-smp 2 \
	-monitor stdio \
	-name "$img" \
	-boot d -cdrom "$iso_path"
