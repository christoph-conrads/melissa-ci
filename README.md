# Melissa Test

This repository contains scripts related to testing Melissa with the aid of
virtual machines and containers.

**WARNING**

Linux Containers (LX**C**) denotes low-level tools to manage containers on Linux. LX**D** is a high-level set of tools for LX**C**. LX**D** contains an executable called "lx**c**".

Usually when web sites talk about LXC and when they show commands like `lxc info`, they are actually relying on LXD.


## GitLab Runner

When GitLab Runner runs are not executed as superuser, then you must add the user `gitlab-runner` to the groups `docker` (when using Docker) and `lxd` (when using LXD), respectively.

To use local images, the configuration must be changed. Open `/etc/gitlab-runner/config.toml` and in the section `runners.docker` add `pull_policy = "never"`:
```toml
[[runner]]
  name = "my-runner-name"
  ...
  [runners.docker]
    tls_verify = false
    image = "ubuntu:20.04"
    ...
    pull_policy = "never"
```

To execute continuous integration jobs locally, execute the following instructions on Maiko for jobs with LXD tag:
```sh
cd git-repository
gitlab-runner exec custom \
    --custom-prepare-exec ~gitlab-runner/gitlab-runner-executor-lxd/prepare.sh \
    --custom-run-exec ~gitlab-runner/gitlab-runner-executor-lxd/run.sh \
    --custom-cleanup-exec ~gitlab-runner/gitlab-runner-executor-lxd/clean-up.sh \
    --builds-dir=/home/john/builds \
    --cache-dir=/home/john/cache \
    'job-name-inside-.gitlab-ci.yml'
```
For jobs tagged Docker, use
```sh
cd git-repository
gitlab-runner exec docker --docker-pull-policy=never 'job-name-inside-gitlab-ci'
```


## LXD

The YAML configurations for distrobuilder in this repository are based on the LXC continuous integration images from the [lxc-ci](https://github.com/lxc/lxc-ci) repository. The YAML files in this repositories can often be customized with parameters that must be passed on the distrobuilder command line. The parameters can be looked up (see [distrobuilder issue #367](https://github.com/lxc/distrobuilder/issues/367))
* in the `jenkins/jobs` directory of the lxc-ci repository,
* the build log files `build.log` on https://images.linuxcontainers.org/images`, or
* in the build logs on https://jenkins.linuxcontainers.org.

How to move the storage pool and disk images on Ubuntu 20:
```sh
sudo apt install lxd
sudo lxd init
sudo mkdir /home/lxd/
sudo chmod go-r /home/lxd/
sudo mv /var/snap/lxd/common/lxd/storage-pools /home/lxd/
sudo ln -sv /home/lxd/storage-pools/ /var/snap/lxd/common/lxd/storage-pools
sudo mv /var/snap/lxd/common/lxd/images/ /home/lxd/
sudo ln -sv /home/lxd/images/ /var/snap/lxd/common/lxd/images
```


## Hints

* You can register a GitLab Runner _without_ having superuser rights; the config can be found in `~/.gitlab-runner`.
* Disable swap partitions inside containers and virtual machines
* How to mount an image:
  * Convert image to raw: `qemu-image convert -p -O raw name.qcow2 name.raw`
  * Determine partition start, sector size: `fdisk -l name.raw`
  * Compute offset: calculate partition start times sector size
  * Mount the partition: `mount -o loop,offset=1048576 name.raw mount-point`
  * For alternatives (e.g., without image conversion) see [_How to mount qcow2 image_](https://unix.stackexchange.com/questions/268460/how-to-mount-qcow2-image)
* How to connect to a QEMU guest over SSH
  * Start the guest SSHD
  * Use QEMU port-forwarding (here from port 5022 to port 22): `-netdev user,hostfwd=tcp::5022-:22`


## Linux Distributions Hints

### Debian-based Distributions (e.g., Ubuntu)

Packages can be installed with the _Advanced Packaging Tool_ (APT). Install software with `apt` or `apt-get`, respectively. `apt` is a more convenient interface to APT in comparison to `apt-get` though they share many command line options. 

Command overview (all commands in this list work with `apt-get`, too):
* Updating the list of packages: `apt update`
* Upgrading packages: `apt upgrade`
* Installing new packages: `apt install package-name`
* Uninstalling packages: `apt remove package-name`, `apt purge package-name` (the latter command also removes configuration files)

The APT repositories contain a convenience package called `build_essential` installing the GCC C and C++ compiler, GNU Make, and standard headers found in `/usr/include`.
```sh
apt install build_essential
```


### Alpine Linux

[Alpine Linux](https://www.alpinelinux.org/) is a security-oriented, lightweight Linux distribution running on a comparatively large number of CPU ISAs including 64-bit little endian PowerPC and ARMv7-A. Alpine Linux is interesting from a build testing perspective because its default libc implementations is musl instead of GNU libc. The package manager is called `apk` (not to be mistaken for the _Android Package Manager_).

Command overview (all commands in this list work with `apt-get`, too):
* Updating the list of packages: `apk update`
* Upgrading packages: TODO
* Installing new packages: `apk add package-name`
* Uninstalling packages: TODO

The apk repositories contain a package called `build-base` containing the GCC C and C++ compiler, GNU Make, and standard headers (i.e., `build-base` is the Alpine-equivalent of Debian's `build_essential` package).

### CentOS

* Updating the list of packages: `dnf check-update`
* Upgrading packages: `dnf update package-name`
* Installing new packages: `dnf install package-name`
* Uninstalling packages: `dnf remove package-name`

To build CentOS distrobuilder image run:

`distrobuilder build-lxd distrobuilder-centos8.yaml -o image.architecture=x86_64 -o image.release=8 -o image.variant=default -o source.variant=boot`


## Acknowledgement

The LXD container image configurations are based on images found in the
[lxc/lxc-ci](https://github.com/lxc/lxc-ci/) repository.
