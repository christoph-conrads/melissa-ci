#!/bin/sh
# Copyright 2020 Institut National de Recherche en Informatique et en Automatique (Inria)

# This cron script checks the on-disk size of an LXD storage pool. If the pool
# consumes too much space, the script deletes STOPPED containers created by the
# GitLab Runner LXD executor in the order of the modification time of the
# directory containing the container state (oldest containers first).

set -e
set -u

runner_regexp='^[[:digit:]]+[[:space:]]+[.][/]runner'
storage_pool='default'
storage_pool_path="/home/lxd/storage-pools/$storage_pool/containers"
max_storage_pool_size_bytes="$(python3 -c 'print(20 * 2**30)')"

cd -- "$storage_pool_path"

container_sizes="$( \
	find . -mindepth 1 -maxdepth 1 -exec ls -1 -dt {} \+ \
	| xargs du --block-size=1 --summarize --total)"


if ! (echo "$container_sizes" | egrep "$runner_regexp" >/dev/null)
then
	echo "no gitlab-runner containers in storage pool $storage_pool"
	exit 0
fi


runner_containers="$(echo "$container_sizes" | egrep "$runner_regexp")"

pool_size_bytes="$(echo "$container_sizes" | fgrep total | awk '{print $1}')"
current_pool_size_bytes=$pool_size_bytes

# POXIX-compliant loop replacing Bash `while read line do ... done <<<input`
echo "$runner_containers" | tac | \
while IFS= read -r line
do
	if [ $current_pool_size_bytes -le $max_storage_pool_size_bytes ]
	then
		break 1
	fi

	container_size="$(echo $line | awk '{print $1}')"
	container="$(echo $line | awk '{print $2}' | sed -e 's:^[.]/::')"
	state="$(lxc ls -c s --format=csv -- "$container")"

	if [ "$state" = "STOPPED" ]
	then
		echo "deleting container $container (size=$container_size)"
		lxc delete -- "$container"

		current_pool_size_bytes=$(((current_pool_size_bytes - $container_size)))
	else
		echo "container $container is not STOPPED"
	fi
done

if [ $current_pool_size_bytes -gt $max_storage_pool_size_bytes ]
then
	>/dev/stderr echo "storage pool $storage_pool still larger than $max_storage_pool_size_bytes bytes"
fi
