#!/usr/bin/python3

# Copyright (c) 2021, Institut National de Recherche en Informatique et en Automatique (Inria)
#               2021, Poznan Supercomputing and Networking Center (https://www.psnc.pl/)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
# TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import argparse
import logging
import re
import subprocess
import sys
import time
from typing import List, Union

# the maximum number of containers is also hardcoded in slurm.conf
MAX_NUM_CONTAINERS = 32


def run(cmd: Union[str, List[str]], **kwargs) -> subprocess.CompletedProcess:
    args = cmd.split() if isinstance(cmd, str) else cmd

    logging.debug(f"run({args}, {kwargs})")

    return subprocess.run(
        args,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True,
        **kwargs)


def run_check(cmd: str, **kwargs) -> subprocess.CompletedProcess:
    try:
        return run(cmd, check=True, **kwargs)
    except Exception as e:
        logging.exception(str(e))
        raise e


def run_silent(cmd: str, **kwargs) -> subprocess.CompletedProcess:
    args = cmd.split()
    return subprocess.run(
        args,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
        universal_newlines=True,
        **kwargs)


def run_p(cmd: str, **kwargs) -> bool:
    return run_silent(cmd, **kwargs).returncode == 0


def lxc_exec(cid: str, cmd: Union[str, List[str]],
             **kwargs) -> subprocess.CompletedProcess:
    args = cmd.split() if isinstance(cmd, str) else cmd
    return run(["lxc", "exec", cid, "--"] + args, **kwargs)


def lxc_exec_check(cid: str, cmd: Union[str, List[str]],
                   **kwargs) -> subprocess.CompletedProcess:
    return lxc_exec(cid, cmd, check=True, **kwargs)


def start_services(cid: str, services: List[str]) -> None:
    logging.info(f"Enabling services {services} in container {cid}")
    for s in services:
        lxc_exec_check(cid, f"systemctl enable {s}")
        lxc_exec_check(cid, f"systemctl start {s}")


def make_container_id(scheduler: str, index: int) -> str:
    assert index >= 0
    cid = "{:s}-{:d}".format(scheduler, index)

    # in order to be able to pass strings instead of list of strings to run(),
    # there must not be any spaces in container names, paths, and so on.
    assert re.search("\s", cid) is None

    return cid


def setup_oar_cluster(num_containers: int, user: str) -> None:
    assert num_containers >= 2
    assert user != ""

    make_cid = lambda index: make_container_id("oar", index)

    #
    # set up OAR server
    #
    master = make_cid(0)
    lecm = lambda cmd, **kwargs: lxc_exec_check(master, cmd, **kwargs)

    start_services(master, ["postgresql"])
    lecm(["oar-database", "--create", "--db-is-local"])
    start_services(master, ["oar-server"])

    for i in range(1, num_containers):
        cid = make_cid(i)
        start_services(cid, ["oar-node"])
        lecm(["sh", "-c", f"echo {cid} >>/tmp/oar-hosts"])

    lecm("oar_resources_init -x -y -o /tmp/oar-init /tmp/oar-hosts")

    # OAR Hello, World!
    lecm([
        "sudo", "--login", "--user", user, "--", "oarsub", "-l", "cpu=1",
        "echo Hello, World!"
    ])
    time.sleep(15)
    lecm(["sudo", "--login", "--user", user, "--", "cat", "OAR.1.stdout"])


def setup_slurm_cluster(num_containers: int, user: str) -> None:
    assert num_containers >= 2
    assert user != ""

    make_cid = lambda index: make_container_id("slurm", index)

    #
    # copy config files into containers
    #
    slurm_config_dir = '/etc/slurm'
    slurm_user = 'root'

    for i in range(num_containers):
        cid = make_cid(i)
        # omit slash between container name and path because slurm_config_dir
        # is an absolute path starting with a slash and LXD 3.0.3 responds with
        #   Error: Missing target directory
        # if `lxc file push` encounters two consecutive slashes.
        #
        # TODO FIX path to config files above if this script is called from
        # directory other than `melissa-ci/virtual-cluster`
        for config in ["slurm.conf", "slurmdbd.conf", "slurmdbd.sql"]:
            assert slurm_config_dir[0] == "/"
            run_check(f"lxc file push -- {config} {cid}{slurm_config_dir}/")
            lxc_exec_check(
                cid,
                f"chown {slurm_user}:{slurm_user} {slurm_config_dir}/{config}")

        lxc_exec_check(cid, f"chmod 600 {slurm_config_dir}/slurmdbd.conf")

    #
    # set up master node
    #
    master = make_cid(0)

    # munge must be started before any Slurm service is started
    start_services(master, ["munge", "mariadb"])

    # "secure" SQL database
    lecm = lambda cmd, **kwargs: lxc_exec_check(master, cmd, **kwargs)
    lecm("mysql", input="DELETE FROM mysql.user WHERE User='';")
    lecm(
        "mysql",
        input=
        "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
    )
    lecm("mysql", input="DROP DATABASE IF EXISTS test;")
    lecm("mysql", input="FLUSH PRIVILEGES;")

    lecm(["bash", "-c", f"mysql -u root <{slurm_config_dir}/slurmdbd.sql"])

    start_services(master, ["slurmdbd"])
    # wait a second before launching slurmctld to ensure slurmdbd is listening;
    # if slurmctld is started too early, it will exit with a non-zero status
    time.sleep(1)
    start_services(master, ["slurmctld"])

    # The setup below relies on the existance of a Slurm cluster called
    # "cluster". Its presence can be checked for with
    #   sacctmgr cluster list
    # On CentOS 8, this cluster exists by default.

    #
    # launch services on slave nodes
    #
    for i in range(1, num_containers):
        cid = make_cid(i)
        start_services(cid, ["munge", "slurmdbd", "slurmctld", "slurmd"])

    #
    # test setup
    #
    lecm([
        "sudo", "--login", "--user", user, "--", "srun", "echo",
        "Hello, World!"
    ])


def main():
    #
    # parse command line
    #
    parser = argparse.ArgumentParser(
        description="Launch a virtual cluster based on LXD containers")
    parser.add_argument(
        "scheduler",
        choices=["oar", "slurm"],
        help="The choice of batch scheduler")
    parser.add_argument(
        "num_containers",
        metavar="num-containers",
        type=int,
        help="the number of containers to launch"
    )
    parser.add_argument("image", help="the LXD image to launch")
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        dest="verbosity",
        default=0,
        help="increase verbosity (default: print warnings, errors)")

    args = parser.parse_args()

    scheduler = args.scheduler
    image = args.image
    num_containers = args.num_containers
    lxd_volume = "shared"
    lxd_storage_pool = "default"

    if num_containers < 2:
        return "need at least two containers, {:d} requested".format(
            num_containers)
    if num_containers > MAX_NUM_CONTAINERS:
        fmt = "this script supports at most {:d} containers, {:d} requested"
        return fmt.format(MAX_NUM_CONTAINERS, num_containers)

    if not run_p(f"lxc storage volume show {lxd_storage_pool} {lxd_volume}"):
        return f"LXD returned an error when querying for volume '{lxd_volume}' in the LXD storage pool '{lxd_storage_pool}'. Please ensure the volume exists."

    if not run_p(f"lxc image info {image}"):
        return f"No information about image '{image}' found. Does the image exist?"

    # set up logging
    log_level = max(logging.ERROR - 10 * (args.verbosity + 1), logging.DEBUG)
    assert log_level <= logging.WARNING
    assert log_level >= logging.DEBUG
    logging.basicConfig(
        format="%(asctime)s %(levelname)-8s %(message)s",
        datefmt="%Y-%m-%dT%H:%M:%S",
        level=log_level)

    #
    # stop (and delete) existing containers, launch new containers
    #
    def make_cid(index: int) -> str:
        return make_container_id(scheduler, index)

    def search_re(regexp: str, text: str):
        return re.search(regexp, text, re.ASCII | re.MULTILINE)

    def container_exists_p(lxc_info: subprocess.CompletedProcess) -> bool:
        assert lxc_info.args[0] == "lxc"
        assert lxc_info.args[1] == "info"
        return lxc_info.returncode == 0

    for i in range(MAX_NUM_CONTAINERS):
        cid = make_cid(i)
        lxc_info = run(f"lxc info -- {cid}")
        status_re = "^Status: (\w+)$"

        if container_exists_p(lxc_info):
            match = search_re(status_re, lxc_info.stdout)
            status = match.group(1)
            if status.upper() == "RUNNING":
                logging.info(f"Stopping container {cid}")
                run_check(f"lxc stop -- {cid}")

    # do not call `lxc exec` in this loop because containers must have finished
    # booting before `lxc exec` can be used
    for i in range(num_containers):
        cid = make_cid(i)
        lxc_info = run(f"lxc info -- {cid}")
        if container_exists_p(lxc_info):
            logging.info(f"Deleting container {cid}")
            run_check(f"lxc delete -- {cid}")

        logging.info(f"Launching container {cid}")
        run_check(f"lxc launch -- {image} {cid}")
        # the path must be the base directory in the LXD image file
        # (hint: home-directory = base-directory / username)
        logging.info(f"Mounting shared volume in container {cid}")
        run_check(
            f"lxc storage volume attach {lxd_storage_pool} {lxd_volume} {cid} /shared"
        )

    #
    # add user shared home directory
    #
    user = "john"
    for i in range(num_containers):
        cid = make_cid(i)
        logging.info(f"Creating home directory of user {user}")
        run_check(f"lxc exec {cid} -- mkdir -p /shared/{user}")
        run_check(f"lxc exec {cid} -- chown -R {user}:{user} /shared/{user}")

    #
    # call scheduler-specific scripts
    #
    if scheduler == "oar":
        setup_oar_cluster(num_containers, user)
    elif scheduler == "slurm":
        setup_slurm_cluster(num_containers, user)
    else:
        raise RuntimeError(f"BUG: setup for scheduler {scheduler} missing")


if __name__ == "__main__":
    sys.exit(main())
