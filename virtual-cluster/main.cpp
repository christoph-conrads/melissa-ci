// Copyright 2020 Institut National de Recherche en Informatique et en Automatique (https://www.inria.fr/)

#if (defined __GNUC__ && __GNUC__ > 7)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-function-type"
#endif
#include <mpi.h>
#if (defined __GNUC__ && __GNUC__ > 7)
#pragma GCC diagnostic pop
#endif
#include <omp.h>

#include <cerrno>
#include <cinttypes>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>

#include <functional>
#include <memory>
#include <type_traits>
#include <vector>


void die(const char* fn) {
	std::perror(fn);
	std::exit(EXIT_FAILURE);
}


int main (int argc, char *argv []) {
	char hostname[257];

	if(gethostname(hostname, sizeof(hostname)-1) < 0) {
		die("gethostname");
	}

	MPI_Init(&argc, &argv);
	auto mpi_finalize = [] (int*) { MPI_Finalize(); };
	auto mpi_guard = std::unique_ptr<int, decltype(mpi_finalize)>(
		&argc, mpi_finalize
	);

	auto mpi_rank = -1;
	MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
	auto mpi_group_size = -1;
	MPI_Comm_size(MPI_COMM_WORLD, &mpi_group_size);
	auto num_procs = omp_get_num_procs();
	auto max_num_threads = omp_get_max_threads();

	std::printf(
		"hostname='%s' group-size=%d rank=%d num-procs=%d max-num-threads=%d\n",
		hostname, mpi_group_size, mpi_rank, num_procs, max_num_threads
	);

	if(mpi_group_size == 1) {
		std::fprintf(stderr, "cannot run RMA demo with group size 1\n");
		std::exit(1);
	}


	// set up MPI RMA
	using BufferType = std::int32_t;
	using Buffer = std::vector<BufferType>;
	auto buffer_size = std::size_t{128};
	auto rma_buffer = Buffer(buffer_size, -1);
	auto mpi_window = MPI_Win{0};
	MPI_Win_create(
		rma_buffer.data(), rma_buffer.size() * sizeof(BufferType),
		sizeof(BufferType),
		MPI_INFO_NULL, MPI_COMM_WORLD, &mpi_window
	);
	auto f = std::function<void(MPI_Win)>(
		[] (MPI_Win p) {
			MPI_Win_free(&p);
		}
	);
	auto mpi_window_guard = std::unique_ptr<
		std::remove_pointer<MPI_Win>::type,
		decltype(f)
	>(mpi_window, f);


	int* p_memory_model = nullptr;
	auto flag = 0;
	if(MPI_Win_get_attr(mpi_window, MPI_WIN_MODEL, &p_memory_model, &flag) < 0) {
		die("MPI_Win_get_attr");
	}

	auto memory_model_str =
		(*p_memory_model == MPI_WIN_SEPARATE) ? "SEPARATE" :
		(*p_memory_model == MPI_WIN_UNIFIED) ? "UNIFIED" : "UNKNOWN"
	;

	if(*p_memory_model != MPI_WIN_UNIFIED) {
		std::fprintf(
			stderr,
			"rank=%d: expected unified memory model (type %d), got %d (%s)\n",
			mpi_rank, MPI_WIN_UNIFIED, *p_memory_model, memory_model_str
		);
		std::exit(EXIT_FAILURE);
	}


	// send, recv
	auto target_rank = (mpi_rank + 1) % mpi_group_size;
	auto source = Buffer(buffer_size, mpi_rank);
	auto count = static_cast<int>(source.size());
	auto target_displacement = MPI_Aint{0};

	MPI_Win_fence(MPI_MODE_NOPRECEDE, mpi_window);
	MPI_Put(
		source.data(), count, MPI_INT32_T,
		target_rank, target_displacement, count, MPI_INT32_T, mpi_window
	);
	MPI_Win_fence(
		MPI_MODE_NOSTORE | MPI_MODE_NOPUT | MPI_MODE_NOSUCCEED,
		mpi_window
	);

	for(auto i = std::size_t{0}; i < buffer_size; ++i) {
		auto expected = (mpi_rank == 0) ? mpi_group_size - 1 : mpi_rank - 1;
		auto actual = rma_buffer[i];

		if(actual != expected) {
			std::fprintf(
				stderr,
				"rank=%d: expected %d at position %zu, got %" PRId32 "\n",
				mpi_rank, expected, i, actual
			);
			break;
		}
	}
}
