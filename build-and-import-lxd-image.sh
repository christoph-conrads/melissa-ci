#!/bin/sh

# Copyright 2020, 2021 Institut National de Recherche en Informatique et en Automatique (Inria)
#
# This script creates LXD images with all Melissa dependencies installed for
# sensitivity analysis ("Melissa" or "Melissa SA" for short) and data
# assimilation (Melissa DA).

# **Reading hints**
#
# * ISA = instruction set architecture
# * "arch" or "architecture" refer to the target instruction set architecture.
#   These two identifiers are used throughout the lxc-ci repository which is
#   why they are used here.
# * The instruction set architecture name "amd64" is used throughout the
#   program instead of the synonym "x86_64.

set -e
set -u


# LXD Distrobuilder yaml files often contain instructions for all maintained
# releases of a distribution. The release can be selected by settings the
# `release` option. The values of this option are often the release codenames
# (e.g., for Debian) or abbreviated codenames (e.g., `bionic` instead of bionic
# beaver for Ubuntu). This function returns LXD release names suitable for the
# yaml files maintained the by LXC developers in the lxc-ci repository
# (https://github.com/lxc/lxc-ci).
get_lxd_release_name() {
	local distribution="${1:?}"
	shift

	for release in "$@"; do
		if [ "$distribution" = 'alpine' \
			-o "$distribution" = 'centos' \
			-o "$distribution" = 'devuan' ]; then
			echo "$release"
		elif [ "$distribution" = 'debian' ]; then
			case "$release" in
				2.0) echo 'hamm' ;;
				2.1) echo 'slink' ;;
				2.2) echo 'potato' ;;
				3.0) echo 'woody' ;;
				3.1) echo 'sarge' ;;
				4.0) echo 'etch' ;;
				5.0) echo 'lenny' ;;
				6.0) echo 'squeeze' ;;
				7) echo 'wheezy' ;;
				8) echo 'jessie' ;;
				9) echo 'stretch' ;;
				10) echo 'buster' ;;
				*)
					>/dev/stderr echo "unknown Debian release '$release'"
					exit 1
					;;
			esac
		elif [ "$distribution" = 'ubuntu' ]; then
			case "$release" in
				18.04) echo 'bionic' ;;
				20.04) echo 'focal' ;;
				20.10) echo 'groovy' ;;
				21.04) echo 'hirsute' ;;
				*)
					>/dev/stderr echo "unknown Ubuntu release '$release'"
					exit 1
					;;
			esac
		else
			>/dev/stderr "unknown distribution '$distribution'"
			exit 1
		fi
	done
}


# "amd64" and "x86_64" are synonyms for 64-bit Intel-compatible instruction set
# architectures. The preferred alias is distribution dependent.
get_architecture_alias() {
	local distribution="${1:?}"
	local arch="${2:?}"

	if [ "$distribution" = 'centos' ] && [ "$arch" = 'amd64' ]; then
		echo 'x86_64'
	elif [ "$distribution" = 'alpine' ] && [ "$arch" = 'amd64' ]; then
		echo 'x86_64'
	else
		echo "$arch"
	fi
}


get_additional_distrobuilder_args() {
	local distribution="${1:?}"
	local release="${2:?}"
	local arch="${3:?}"

	if [ "$distribution" = 'centos' ] && [ "$release" -eq 8 ]; then
		echo -o source.variant=boot
	fi
}


run_distrobuilder() {
	local distribution="${1:?}"
	local release="${2:?}"
	local arch="${3:?}"
	local config="${4:?}"
	local command="${5:?}"
	shift 5

	local arch_alias="$(get_architecture_alias "$distribution" "$arch")"
	local codename="$(get_lxd_release_name "$distribution" "$release")"

	distrobuilder "$command" "$config" \
		--cache-dir distrobuilder-cache/ \
		--timeout 600 \
		-o image.release="$codename" \
		-o image.architecture="$arch_alias" \
		-o image.serial="datamove-$(date '+%Y%m%d')" \
		$(get_additional_distrobuilder_args "$distribution" "$release" "$arch")\
		$@
}


image_config="${1:?YAML file with LXD image config needed}"
release="${2:?Distribution release needed}"
architecture="${3:?Instruction set architecture needed}"
image_alias="${4:?LXD image alias needed}"
pdaf_tarball="${5:?Path to PDAF 1.15 tarball needed}"

distribution="$(basename "$image_config" .yaml)"
codename="$(get_lxd_release_name "$distribution" "$release")"

known_archs='aarch64 amd64 armhf i386'
if ! $(echo $known_archs | fgrep --quiet --word-regexp -- "$architecture"); then
	>/dev/stderr echo "unknown instruction set architecture '$architecture'"
	exit 1
fi

if [ "$distribution" = centos ] && [ "$architecture" = i386 ]; then
	echo 'As of February 6, 2021, Distrobuilder downloads the CentOS 7 tarball'
	echo 'for the i386 architecture over an insecure HTTP connection without'
	echo 'verifying its cryptographic signature.'
	echo 'For this reason CentOS 7 (i386) images will not be built.'
	exit 1
fi

if [ -z $(which distrobuilder) ]; then
	>/dev/stderr echo "distrobuilder not found in PATH"
	exit 1
fi


run_distrobuilder "$distribution" "$release" "$architecture" "$image_config" \
	build-dir rootfs

tar --no-same-owner -C rootfs/home/john -xf "$pdaf_tarball"
git \
	-c advice.detachedHead=false \
	-C rootfs/home/john/ \
	clone --quiet --depth=1 --branch=melissa \
	-- 'https://github.com/leobago/fti.git'
# assumption: john has uid, gid 1000
chown -R 1000:1000 -- rootfs/home/john/


run_distrobuilder "$distribution" "$release" "$architecture" "$image_config" \
	pack-lxd rootfs .


# explicitly delete existing images for otherwise the old image will be kept
# without alias
if $(lxc image info "$image_alias" >/dev/null 2>/dev/null)
then
	echo "deleting existing LXD image $image_alias..."
	lxc image delete -- "$image_alias"
fi

echo "importing LXD image $image_alias..."
lxc image import --alias "$image_alias" -- lxd.tar.xz rootfs.squashfs
